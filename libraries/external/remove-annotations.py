#!/usr/bin/env python

#    This file is part of GetSetFMUstateEval
#    Copyright (C) 2020  Stefano Sinisi
#
#    GetSetFMUstateEval is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    GetSetFMUstateEval is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GetSetFMUstateEval.
#    If not, see <https://www.gnu.org/licenses/>.


import os, sys

def clean_annotations(filename):
    with open(filename, "r") as fp:
        content = fp.read()

    while "annotation(" in content:
        noteidx = content.find("annotation(")
        endl = content.find(");", noteidx)
        content = content[:noteidx] + content[endl + 2:]

    with open(filename, "w") as fp:
        fp.write(content)


if len(sys.argv) < 2:
    sys.stderr.write("Not enough arguments.")
directory  = sys.argv[1]
mo_files = os.listdir(directory)
for name in mo_files:
    clean_annotations('{}/{}'.format(directory,name))
    # print('{}/{}'.format(directory,name))

