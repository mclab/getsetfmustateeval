#!/usr/bin/env bash

LIBRARY=$1
output="output"
input="libraries/${LIBRARY}"

num_models=$(wc -l libraries/${LIBRARY}.csv | awk '{print $1}')

for i in $(seq 0 ${num_models}); do

  readarray models < libraries/${LIBRARY}.csv

  case ${LIBRARY} in

    BMD)
      model_name=$(echo ${models[${i}]} | awk '{print $2}')
      model_files=$(echo ${models[${i}]} | awk '{print $1}')
      model="${input}"/${model_files}
      stoptime=100
      tolerance=0.000001
      solstepsize=0.001
      output="output/${LIBRARY}/${model_name}"
      libraries/external/remove-annotations.py "${model}"
      mkdir -p "${output}"
      echo "bin/test-fmueval ${model_name} "${model}" --maxh ${solstepsize} --rtol ${tolerance} -s ${stoptime} -o "${output}" --solver CVode"
      bin/test-fmueval ${model_name} "${model}" --maxh ${solstepsize} --rtol ${tolerance} -s ${stoptime} -o "${output}" --solver CVode > ${output}/out 2>&1 && echo "Done"
      echo "bin/test-performance ${output}/execution-time.csv > ${output}/perf.csv"
      bin/test-performance "${output}"/execution-time.csv > "${output}"/perf.csv && echo "Done"
      ;;

    MSL)
      model_name=$(echo ${models[${i}]} | awk -F' ' '{print $1}')
      model_files=$(echo ${models[${i}]} | awk -F' ' '{printf("%s",$2)}')
      stoptime=$(echo ${models[${i}]} | awk -F' ' '{print $5}')
      tolerance=$(echo ${models[${i}]} | awk -F' ' '{print $6}')
      solstepsize=$(echo ${models[${i}]} | awk -F' ' '{print $7}')
      model="${input}"/Modelica/package.mo
      output="output/${LIBRARY}/${model_name}"
      mkdir -p "${output}"
      echo "bin/test-fmueval ${model_name} "${model}" --maxh ${solstepsize} --rtol ${tolerance} -s ${stoptime} -o "${output}" --solver CVode"
      bin/test-fmueval ${model_name} "${model}" --maxh ${solstepsize} --rtol ${tolerance} -s ${stoptime} -o "${output}" --solver CVode > ${output}/out 2>&1 && echo "Done"
      echo "bin/test-performance ${output}/execution-time.csv > ${output}/perf.csv"
      bin/test-performance "${output}"/execution-time.csv > "${output}"/perf.csv && echo "Done"
      ;;

    STS)
      model_name=$(echo ${models[${i}]} | awk -F' ' '{print $1}')
      model_files=$(echo ${models[${i}]} | awk -F' ' '{printf("%s",$2)}')
      stoptime=$(echo ${models[${i}]} | awk -F' ' '{print $5}')
      tolerance=$(echo ${models[${i}]} | awk -F' ' '{print $6}')
      solstepsize=$(echo ${models[${i}]} | awk -F' ' '{print $7}')
      model="${input}"/ScalableTestSuite/package.mo
      output="output/${LIBRARY}/${model_name}"
      mkdir -p "${output}"
      echo "bin/test-fmueval ${model_name} "${model}" --maxh ${solstepsize} --rtol ${tolerance} -s ${stoptime} -o "${output}" --solver CVode"
      bin/test-fmueval ${model_name} "${model}" --maxh ${solstepsize} --rtol ${tolerance} -s ${stoptime} -o "${output}" --solver CVode > ${output}/out 2>&1 && echo "Done"
      echo "bin/test-performance ${output}/execution-time.csv > ${output}/perf.csv"
      bin/test-performance "${output}"/execution-time.csv > "${output}"/perf.csv && echo "Done"
      ;;
  esac

done
