
# GetSetFMUstateEval

This repo collects tools needed to evaluate correctness and performance of FMI 2.0 functionalities to save and restore internal FMU states.

## Dependencies

* JModelica.org 2.1 extended with save-and-restore state functionality, [https://bitbucket.org/mclab/jmodelica.org](https://bitbucket.org/mclab/jmodelica.org)

## Run

To run our tools on FMUs within BioModels (BMD), Modelica Standard Library (MSL) or ScalableTestSuite (STS) execute the following command:

```
./run.sh {BMD, MSL, STS}
```

Output of the above command will be stored in `output` folder.


## Contributors

* [Stefano Sinisi](linkedin.com/in/stefanosinisi)

## License

GetSetFMUstateEval is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as published
by the Free Software Foundation.

GetSetFMUstateEval is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GetSetFMUstateEval.
If not, see [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).
