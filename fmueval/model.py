
#    This file is part of GetSetFMUstateEval
#    Copyright (C) 2020  Stefano Sinisi
#
#    GetSetFMUstateEval is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    GetSetFMUstateEval is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GetSetFMUstateEval.
#    If not, see <https://www.gnu.org/licenses/>.


import time
import logging
import math
import copy
import numpy as np

import pymodelica
import pyfmi

def interp_nearest(x, xp, fp):
    step_size = xp[1] - xp[0]
    y  = []
    for x_elem in x:
        fp_i = int((x_elem - xp[0]) / step_size)
        while fp_i >= len(fp) or xp[fp_i] > x_elem:
            fp_i -= 1
        if fp_i == len(fp) - 1:
            y.append(fp[fp_i])
            continue
        while fp_i < len(fp) - 1 and xp[fp_i + 1] <= x_elem:
            fp_i += 1
        y.append(fp[fp_i])
    return y


def compare_solution(x, y, solution_step_size, atol=1e-6, rtol=1e-6):
    mean_rel_error = []
    mean_abs_error = []
    rmse = []
    mean_val = []
    min_val = []
    max_val = []
    isclose = [True] * len(y.var_names)
    bitwise = [True] * len(y.var_names)
    # timepoints = np.arange(0, y.solution[0][-1], solution_step_size / 10.0)
    # atol_np = np.full(len(timepoints), atol)
    atol_np = np.full(len(y.solution[0]), atol)
    for i, name in enumerate(y.var_names):
        x_i = x.solution[i]
        # x_i = np.interp(timepoints, x.solution[0], x.solution[i])
        y_i = y.solution[i]
        # y_i = np.interp(timepoints, y.solution[0], y.solution[i])
        abs_error = np.abs(x_i - y_i)
        abs_square_error = np.square(abs_error)
        abs_error_tolerance = np.array([ v if v > 0 else 0 for v in abs_error - np.maximum(atol_np, rtol * np.abs(y_i))])
        abs_square_error_tolerance = np.array([ v if v > 0 else 0 for v in abs_square_error - np.maximum(atol_np, rtol * np.abs(y_i))])

        rel_error = np.array(abs_error_tolerance / (np.abs(y_i) + np.maximum(atol_np, rtol * np.abs(y_i))))

        mean_rel_error.append(np.mean(rel_error))
        mean_abs_error.append(np.mean(abs_error_tolerance))
        max_val.append(np.max(y_i))
        min_val.append(np.min(y_i))
        mean_val.append( np.mean(y_i))
        rmse.append(np.sqrt(np.mean(abs_square_error_tolerance)))
        isclose[i] = isclose[i] and np.all(abs_error <= np.maximum(atol_np, rtol * np.abs(y_i)))
        bitwise[i] = bitwise[i] and np.array_equal(x_i, y_i)
    return mean_rel_error, mean_abs_error, rmse, mean_val, max_val, min_val, isclose, bitwise


class Model(object):
    def __init__(self):
        self._logger = logging.getLogger('fmueval.model.Model')
        self.execution_time = 0.0

    def load_fmu(self, fmu_path, sampling_time, solution_step_size, var_names=None, atol='Default', rtol='Default', solver='CVode'):
        pass

    def run(self, duration):
        pass

    def get(self, name):
        pass

    def get_state(self):
        pass

    def set_state(self, s):
        pass

    def save_solution(self, fname):
        np.savetxt(fname,
                   np.transpose(self.solution),
                   delimiter='\t',
                   header='\t'.join(self.var_names))

    def load_solution(self, fname):
        self.solution = np.genfromtxt(fname, delimiter='\t', names=True)
        self.var_names = [n for n in self.solution.dtype.names]
        self.solution = [self.solution[n] for n in self.var_names]


class PyFMIModel(Model):
    def __init__(self):
        Model.__init__(self)

    def load_fmu(self, fmu_path, sampling_time, solution_step_size, var_names=None, atol='Default', rtol='Default', solver='CVode'):
        self._fmu = pyfmi.load_fmu(fmu_path, log_file_name='log.txt', log_level=7)
        self.sampling_time = sampling_time
        self.solution_step_size = solution_step_size
        self.time = 0
        self.initialized = False
        self.opts = self._fmu.simulate_options()
        if solver == 'CVode':
            self.opts['CVode_options']['verbosity'] = 50
            self.opts['CVode_options']['atol'] = atol
            self.opts['CVode_options']['rtol'] = rtol
            self.opts['CVode_options']['maxh'] = self.sampling_time
            self.opts['CVode_options']["store_event_points"] = False
        elif solver == 'ExplicitEuler':
            self.opts['solver'] = 'ExplicitEuler'
            self.opts['ExplicitEuler_options']['h'] = self.sampling_time
        elif solver == 'RungeKutta34':
            self.opts['solver'] = 'RungeKutta34'
            self.opts['RungeKutta34_options']['atol'] = atol
            self.opts['RungeKutta34_options']['rtol'] = rtol
        else:
            raise pyfmi.common.algorithm_drivers.InvalidSolverArgumentException(solver)
        self.opts['result_handling'] = 'memory'
        self.var_names = ['time']
        if var_names is None:
            for var_type in [0,1,2,4]:
                for var_causality in [3,4,5,6]:
                    for var_variability in [3,4]:
                        model_var = list(self._fmu.get_model_variables(type=var_type, include_alias=False, causality=var_causality, variability=var_variability).keys())
                        if model_var is not []:
                            self.var_names = self.var_names + model_var
        else:
            self.var_names = var_names
        self.solution = None

    def run(self, duration):
        if duration == 0:
            return
        event_update_time = 0.0
        self.opts['ncp'] = int(math.ceil(duration / self.solution_step_size)) if self.solution_step_size > 0 else 0
        if self.initialized:
            self.opts['initialize'] = False
            event_update_time = time.time()
            # try:
            #     self._fmu.enter_event_mode()
            # except Exception as e:
            #     pass
            self._fmu.event_update()
            # self._fmu.enter_continuous_time_mode()
            event_update_time = time.time() - event_update_time
        else:
            self.opts['initialize'] = True
            self.initialized = True
        start_time = self.time
        final_time = start_time + duration
        self._logger.info("Run from {} to {}".format(start_time, final_time))
        run_time = time.time()
        res = self._fmu.simulate(start_time = start_time,
                                 final_time = final_time,
                                 options = self.opts)
        self.execution_time = time.time() - run_time + event_update_time
        self.time = self._fmu.time
        if self.solution is None:
            self.solution = [res[name] for name in self.var_names]
        else:
            for i, name in enumerate(self.var_names):
                self.solution[i] = np.concatenate((self.solution[i], res[name][1:]))

    def get(self, name):
        return self._fmu.get(name)[0]

    def get_state(self):
        self.execution_time = time.time()
        s = self._fmu.get_fmu_state()
        self.execution_time = time.time() - self.execution_time
        return {
            'fmu': s, 
            'initialized': self.initialized,
            'time': self.time,
            'solution': copy.deepcopy(self.solution)
        }

    def set_state(self, s):
        self.execution_time = time.time()
        self.initialized = s['initialized']
        self.time = s['time']
        # self._fmu.time = self.time
        self._fmu.set_time__t(self.time)
        self._fmu.set_fmu_state(s['fmu'])
        self.execution_time = time.time() - self.execution_time
        self.solution = copy.deepcopy(s['solution'])

    def free_state(self, s):
        self.execution_time = time.time()
        self._fmu.free_fmu_state(s['fmu'])
        self.execution_time = time.time() - self.execution_time

    
