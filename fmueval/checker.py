
#    This file is part of GetSetFMUstateEval
#    Copyright (C) 2020  Stefano Sinisi
#
#    GetSetFMUstateEval is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    GetSetFMUstateEval is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GetSetFMUstateEval.
#    If not, see <https://www.gnu.org/licenses/>.

import sys
import os
import logging
import pymodelica
import pyfmi
import numpy as np
import random
import time
import pickle

from .model import compare_solution

from .model import PyFMIModel


def comparison(model_ref, model_to_compare, output, logger=logging.getLogger('fmueval.check.compare')):
	rel_error, abs_error, rmse, mean_val, max_val, min_val, isclose, bitwise = compare_solution(model_to_compare, model_ref, model_ref.solution_step_size)
	den = [ max_val_i - min_val_i if max_val_i - min_val_i >= sys.float_info.epsilon else sys.float_info.epsilon for max_val_i,min_val_i in zip(max_val, min_val)]
	with open('{}/comparison.csv'.format(output), "w") as output_file:
		output_file.write('name, rel_error, abs_error, rmse, mean, min, max, isclose, bitwise, nrmse\n')
		for name, rel_error_i, abs_error_i, rmse_i, mean_val_i, min_val_i, max_val_i, isclose_i, bitwise_i, den_i in zip(model_ref.var_names, rel_error, abs_error, rmse, mean_val, min_val, max_val, isclose, bitwise, den):
			output_file.write('{}, {}, {}, {}, {}, {}, {}, {}, {}, {}\n'.format(name, rel_error_i, abs_error_i, rmse_i, mean_val_i, min_val_i, max_val_i, isclose_i, bitwise_i, rmse_i / den_i))
	nrmse = np.divide(rmse, den)
	max_nrmse = np.max(nrmse)
	logger.info('Average MAPE among model variables: {:.17g}'.format(np.sum(rel_error) / len(rel_error)))
	logger.info('Average NRMSE among model variables: {:.17g}'.format(np.mean(nrmse)))
	logger.info('Max NRMSE among model variables: {:.17g}'.format(max_nrmse))
	logger.info('allclose: {}'.format(np.all(isclose)))
	logger.info('bitwise: {}'.format(np.all(bitwise)))


def check(fmu_path, sampling_time, solution_step_size, step_size, num_steps, output, atol, rtol, random_state_file=None, solver='CVode'):
	logger = logging.getLogger('fmueval.check')
	if random_state_file is None:
		with open('{}/random.state'.format(output), 'w') as f:
			pickle.dump(random.getstate(),f)
	else:
		random.setstate(pickle.load(random_state_file))
	# single run
	# ___model = PyFMIModel()
	# ___model.load_fmu(fmu_path, sampling_time, solution_step_size)
	# var_names = ___model.var_names

	model_singlerun = PyFMIModel() #FMPyModel()
	# model_singlerun.load_fmu(fmu_path, sampling_time, solution_step_size, var_names=var_names, atol=atol, rtol=rtol, solver=solver)
	model_singlerun.load_fmu(fmu_path, sampling_time, solution_step_size, atol=atol, rtol=rtol, solver=solver)
	var_names = model_singlerun.var_names

	model_run_up_to = PyFMIModel() #FMPyModel()

	# sequence run
	model_seqrun = PyFMIModel() #FMPyModel()
	model_seqrun.load_fmu(fmu_path, sampling_time, solution_step_size, var_names=var_names, atol=atol, rtol=rtol, solver=solver)
	# model_seqrun.load_fmu(fmu_path, sampling_time, solution_step_size, atol=atol, rtol=rtol, solver=solver)
	run_up_to_execution_time = []
	run_execution_time = []
	get_execution_time = []
	set_execution_time = []
	free_execution_time = []
	for i in range(0, num_steps):
		logger.info('Current simulation step: {}'.format(i))

		model_run_up_to.load_fmu(fmu_path, sampling_time, solution_step_size, var_names=var_names, atol=atol, rtol=rtol)
		model_run_up_to.run((i + 1) * step_size)
		run_up_to_execution_time.append(model_run_up_to.execution_time)

		logger.info('Run for {}'.format(step_size))
		model_singlerun.run(step_size)
		logger.info('Run for {}: done!'.format(step_size))

		logger.info('Run for {}'.format(step_size))
		model_seqrun.run(step_size)
		run_execution_time.append(model_seqrun.execution_time)
		logger.info('Run for {}: done!'.format(step_size))

		logger.info("--> store fmu state")
		s = model_seqrun.get_state()
		get_execution_time.append(model_seqrun.execution_time)
		logger.info("--> store fmu state: done!")

		rnd_duration = random.random() * step_size * num_steps
		logger.info("--> random simulation of length {}".format(rnd_duration))
		model_seqrun.run(rnd_duration)
		logger.info("--> random simulation of length {}: done!".format(rnd_duration))

		logger.info("--> load back fmu state")
		model_seqrun.set_state(s)
		set_execution_time.append(model_seqrun.execution_time)
		logger.info("--> load back fmu state: done!")

		logger.info("--> free fmu state")
		model_seqrun.free_state(s)
		free_execution_time.append(model_seqrun.execution_time)
		logger.info("--> free fmu state: done!")

	model_singlerun.save_solution('{}/single-run.csv'.format(output))
	model_seqrun.save_solution('{}/sequence-run.csv'.format(output))
	comparison(model_singlerun, model_seqrun, output)
	with open('{}/execution-time.csv'.format(output), 'w') as f:
		f.write('step,run,get,set,free,run_up_to_step\n')
		for i, elem in enumerate(zip(run_execution_time, get_execution_time, set_execution_time, free_execution_time, run_up_to_execution_time)):
			f.write('{},'.format(i))
			f.write(','.join(['{}'.format(i) for i in elem]))
			f.write('\n')
