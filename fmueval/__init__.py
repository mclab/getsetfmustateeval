
#    This file is part of GetSetFMUstateEval
#    Copyright (C) 2020  Stefano Sinisi
#
#    GetSetFMUstateEval is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    GetSetFMUstateEval is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GetSetFMUstateEval.
#    If not, see <https://www.gnu.org/licenses/>.


import logging
module_logger = logging.getLogger('fmueval')
module_logger.addHandler(logging.NullHandler())
