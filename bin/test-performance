#! /usr/bin/env python

#    This file is part of GetSetFMUstateEval
#    Copyright (C) 2020  Stefano Sinisi
#
#    GetSetFMUstateEval is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published
#    by the Free Software Foundation.
#
#    GetSetFMUstateEval is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GetSetFMUstateEval.
#    If not, see <https://www.gnu.org/licenses/>.


import sys
sys.path.append('.')

import argparse
import math
import numpy as np
import pprint

parser = argparse.ArgumentParser(description='FMUSetGet Performance Computation')

parser.add_argument('input')

parser.add_argument('-d', '--max_depth', type=int, default=100, help="Maximum depth")
parser.add_argument('-b', '--max_branching_factor', type=int, default=10, help="Maximum branching factor")
parser.add_argument('-o', '--output', type=str, help='Output directory', default='.')
args = parser.parse_args()

execution_times = np.genfromtxt(args.input, delimiter=',', names=True)

times = {}
for b in xrange(1, args.max_branching_factor+1):
	optimized_search = 0.0
	times[b] = {}
	nodes_total = 0
	for h in xrange(1, args.max_depth+1):
		nodes_h = b**h
		nodes_pre_h = b**(h-1)
		nodes_total += nodes_h
		run_up_to_step_t = execution_times['run_up_to_step'][h if h < 100 else h-1]
		set_t = execution_times['set'][h-1]
		get_t = execution_times['get'][h-1]
		free_t = execution_times['free'][h-1]
		run_t = execution_times['run'][h-1]
		canonical_search = run_up_to_step_t * nodes_h
		optimized_search += (get_t + free_t + set_t * b + run_t * b) * nodes_pre_h
		speed_up_t = canonical_search / optimized_search
		slow_down_t = optimized_search / canonical_search
		times[b][h] = {'nodes': nodes_total, 'canonical_search': canonical_search, 'optimized_search': optimized_search, 'speed_up': speed_up_t, 'slow_down': slow_down_t}
		print(','.join(['{:g}'.format(v) for v in [b, h, nodes_total, canonical_search, optimized_search, speed_up_t, slow_down_t]]))
# pprint.pprint(times)
